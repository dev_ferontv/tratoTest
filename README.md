# Technical Test

## Javascript

1. What is a **callback**?
R: A function which is passed as an argument in other function, and the second function is called later to complete some task, nesting many function as arguments if it is required.
2. What's the difference between: var x = 1 and x = 1
R: First option variable is created and a value is assigned, second option just asigns a value without creating the variable.
3. What's the meaning of *this*?
R: Value of *this* makes reference to function owner in which it is invocated or refers to the object's method. When we call a function with keyword `new`, implicitly Javascript returns `this`.
4. What's the usage of the command `typeof`?
R: Gives us a string that shows us which type is our declaration.
5. What's the usage of the command `instanceof`?
R: This object verifies object type in some sort of time. It is used in exeptions to show if it belongs to some type of object or not.
6. How can you evaluate if a variable is `null`?
R: `null` is a value that can be assigned to a variable, so we can directly evaluate with a conditional or other operator if any variable is null.
7. How can you evaluate if a variable is `undefined`?
R: If a variable is declared (or never declared) but a value is never assigned, it will be undefined.
8. What are `closures`?
R: Closures are inner functions which use variables declared at same level (variables defined outside the function).
9. What's the usage of the commands `apply` and `call`
R: The usage is to write a method that can be used on diferent objects. `call()` method takes arguments separately and `apply()` takes arguments as an array.
10. Write a function that receives `n` arguments and returns the sum of all of them?
```
function sumAll (..args){
  return args.reduce((a, b) => { return a + b; });
}

sortArgs(10, 20, 30, 40); // Prints 100
```

11. Write a small algorithm in Javascript that returns the following object. (Must be automatic)
```
{
    key1: 1,
    key2: 2,
    key3: 3,
    key4: 4
}
```
R:
```
function smartJson(number){
  let obj = {}
  for(var i = 1; i <= number; i++){
    obj["key"+ i] = i;
  }
  
  return obj;
}

console.log(smartJson(5));
```

12. Using the following object, write the instruction that removes the value 'black' from it and removes the attribute 'model'.

R:

```
var obj = {
    make: 'Mazda',
    model: 'Mazda 3',
    plates: '456HJK',
    color: 'black'
}

function selectSome(){
  obj.color = "";
  obj[''] = obj.model;
  delete obj.model;
  return obj;
}

console.log(selectSome());
```

13. Write an algorithm that send through ajax each element from the following array. **Note: Each request must be made once the previous has finished.**

    ['This', 'is', 'a', 'fake', 'array']

R:
```
var params = ['This', 'is', 'a', 'fake', 'array']

function postEach() {
  let xhttp = new XMLHttpRequest();
  let url = 'test.php';
  
  params.forEach(function(element) {
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');    
    http.onreadystatechange = function() {
      if(http.readyState == 4 && http.status == 200) {
          alert(http.responseText);
      }
    }
    http.send(element);
  });
}

postEach();
```

## Javascript

1. What is Node.js?
R: Node.js is a code execution environment running Javascript line-to-line. It is developed each day by a huge community helping to use Javascript in many ways (beyond web clients) changing language paradigm.
2. What is the programming language that Node.js is based?
R: Programmed in Javascript and C++, used with Javascript.
3. What is npm?
R: Default package manager for Node.js
4. What does module.exports do?
R: Basically it exposes a module to be imported and used by other document. `module.exports` is included in every Node.js app by default.
5. How can you import a Node.js library in an application?
R: There are many ways to import (with advantages/disadvantages and requirements), to mention:
**CommonJS**
var module = require('module');
**AMD**
define(['module1', ',module2'], function(module1, module2) {
  console.log(module1.setName());
});
**ECMAScript 6 modules (Native JavaScript)**
import { module } from 'module;
6. How can you install a Node.js package?
R: Once we have npm initialized in our project, we can install a pakage with a simple command: `npm install <name-of-the-package>`
7. All Node.js apis are:
 - Asyncronous
 - Syncronous
 - Both ---------- CORRECT 
 - None of the above
8. In which of the following scenarios is a good idea to use Node.js?
 - I/O applications
 - Data streaming applications
 - Realtime data transfer applications
 - All of the above ----------- CORRECT
9. What is `console.log`?
R: This method is used to show defined messages in console. Useful for testing.
10. What is `console.error`?
R: This method is used to show error messages in console.
11. What is `console.time`?
R: This method tracks how much time takes to finish an operation.
12. What is ExpressJS?
R: Express.js is a framework to develop web apps, apis and web services. This framework is open source with MIT licence.

## Web Application
1. Deploy a back-end solution that allows to read, write, delete, update and insert into a DB. Consider the following statements.
Use Node.js
Validate all kind of exceptions

The back-end implementation must be with REST Services (GET, POST, DELETE, PUT).

**Go inside ```cd ./serverTratoio```, install dependencies with ```npm i``` and run with run ```npm run dev```**

Requests:

GET CONTRACTS localhost:3000/api/contracts
GET CONTRACT localhost:3000/api/contracts/${id}
POST CONTRACT localhost:3000/api/contracts
  L BODY > raw > JSON:

  {
    title: "New Car Insurance",
    owner: "Ben Affleck",
    bank: "Barclays",
    country: "UK"
  }

PUT CONTRACT localhost:3000/api/contracts/${id}
  L BODY > raw > JSON:

  {
    id: 5
    title: "New Motorcycle Insurance",
    owner: "Chris Evans",
    bank: "BBVA",
    country: "SP"
  }

DELETE CONTRACT localhost:3000/api/contracts/${id}