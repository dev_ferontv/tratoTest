const contractsMocks = require('../utils/mocks/contracts');

class ContractsService {
    constructor() {

    }

    getContracts({ tags }) {
        return Promise.resolve(contractsMocks);
    }

    getContract({ contractId }) {
        return Promise.resolve(contractsMocks[0]);
    }

    createContract({ contract }) {
        return Promise.resolve(contractsMocks[0]);
    }

    updateContract({ contractId, contract }) {
        return Promise.resolve(contractsMocks[0]);
    }

    deleteContract({ contractId }) {
        return Promise.resolve(contractsMocks[0]);
    }
}

module.exports = ContractsService;