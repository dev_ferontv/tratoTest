const express = require("express");
const router = express.Router();
const ContractsService = require("../../services/contracts");

const contractService = new ContractsService();

router.get("/", async function(req, res, next) {
  const { tags } = req.query;

  console.log("req", req.query);

  try {
    const contracts = await contractService.getContracts({ tags });

    res.status(200).json({
      data: contracts,
      message: "contracts listed"
    });
  } catch (err) {
    next(err);
  }
});

router.get("/:contractId", async function(req, res, next) {
  const { contractId } = req.params;

  console.log("req", req.params);

  try {
    const contract = await contractService.getContract({ contractId });

    res.status(200).json({
      data: contract,
      message: "contract retrieved"
    });
  } catch (err) {
    next(err);
  }
});

router.post("/", async function(req, res, next) {
  const { body: contract } = req;

  console.log("req", req.body);

  try {
    const createdContract = await contractService.createContract({ contract });

    res.status(201).json({
      data: createdContract,
      message: "contract created"
    });
  } catch (err) {
    next(err);
  }
});

router.put("/:contractId", async function(req, res, next) {
  const { contractId } = req.params;
  const { body: contract } = req;

  console.log("req", req.params, req.body);

  try {
    const updatedContract = await contractService.updateContract({
      contractId,
      contract
    });
    res.status(200).json({
      data: updatedContract,
      message: "contract updated"
    });
  } catch (err) {
    next(err);
  }
});

router.delete("/:contractId", async function(req, res, next) {
  const { contractId } = req.params;

  console.log("req", req.params);

  try {
    const deletedContract = await contractService.deleteContract({ contractId });

    res.status(200).json({
      data: deletedContract,
      message: "contract deleted"
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;