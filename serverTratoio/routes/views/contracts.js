const express = require('express');
const router = express.Router();
const ContractService = require('../../services/contracts');

const contractService = new ContractService();

router.get("/", async function(req, res, next) {
  const { tags } = req.query;
  try {
    const contracts = await contractService.getContracts({ tags });
    res.render("contracts", { contracts });
  } catch (err) {
    next(err);
  }
});

module.exports = router;