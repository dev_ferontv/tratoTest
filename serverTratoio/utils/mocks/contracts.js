const contractMocks = [
  {
    title: "Open a bank account",
    owner: "Ryan Reynolds",
    bank: "Citi",
    country: "US"
  },
  {
    title: "New Life Insurance",
    owner: "Ryan Gosling",
    bank: "HSBC",
    country: "UK"
  },
];

module.exports = contractMocks;